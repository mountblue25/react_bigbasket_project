import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export class SaleAndBankOffer extends Component {
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-12 d-none d-lg-block border border-1 rounded mt-5'>
                        <img src='https://www.bigbasket.com/media/uploads/banner_images/hp_bh_m_pbs_entrypoint-400_010922.jpg' className='w-100' />
                    </div>
                    <div className='col-12 ps-0 mt-5'>
                        <h3 className='text-dark'>Bank Offers</h3>
                        <hr />
                    </div>
                    <div className='col-lg-3 col-6 mb-2'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/7dd00b6f-f6ea-483d-933b-3f5c8dd1f694/t1_hp_aff_m_paytm_360_060922.jpg' alt='bank' className='w-100 h-100 ms-0 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3 col-6 mb-2'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/7dd00b6f-f6ea-483d-933b-3f5c8dd1f694/t1_hp_aff_m_one_360_060922.jpg' alt='bank' className='w-100 h-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/7dd00b6f-f6ea-483d-933b-3f5c8dd1f694/t1_hp_aff_m_icici_360_060922.jpg' alt='bank' className='w-100 h-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-lg-3 col-6'>
                        <img src='https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/0c1dea6d-a98b-470b-9df5-9991a2f1356c/7dd00b6f-f6ea-483d-933b-3f5c8dd1f694/t1_hp_m_aff_dbs_360_060922.jpg' alt='bank' className='w-100 h-100 me-2 border border-secondary border-opacity-10' />
                    </div>
                    <div className='col-12 d-flex flex-row justify-content-between pt-3 shadow-sm mt-4'>
                        <h3 className='text-dark mx-auto'>Best Sellers</h3>
                        <Link to='/displayproducts'>
                            <button className='btn btn-success btn-sm float-end border border-0' style={{ backgroundColor: '#84c225' }}>ShowMore</button>
                        </Link>
                    </div>
                    <div className='col-lg-3 col-md-4 col-6 border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/5`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 3% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/312169_13-britannia-bake-rusk-toast.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='text-wrap text-dark'>Britannia Toastea Rusk Toast - Crispy Baked, Premium Quality Teatime Snack, 200 g</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>200g-Rs.34.00</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.35</s><span className='text-dark'>Rs.34</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 mt-2 ms-3'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-lg-3 col-md-4 col-6 border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/6`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 21% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/240067_19-tata-tea-gold-tea.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='text-wrap text-dark'>Tata Tea Gold Assam Teas With Gently Rolled Aromatic Long Leaves, Rich & Aromatic Chai, Black Tea, 500 g</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>500g-Rs.250</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.315</s><span className='text-dark'>Rs.250</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 mt-2 ms-3'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-lg-3 col-md-4 col-6 border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/7`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 13% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/40005958_2-gowardhan-fresh-paneer-classic-block.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='mb-5 p-0 text-dark'>Gowardhan Fresh Paneer - Classic Block, 200 g Pouch</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>200g-Rs.85.00</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.98</s><span className='text-dark'>Rs.85</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 ms-3 mt-2'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-lg-3 col-6 d-md-none d-lg-block border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/8`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 24% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/266579_25-bru-instant-coffee.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='text-wrap mb-5 pb-3 text-dark'>BRU Instant Coffee, 200 g Pouch</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>200g-Rs.380.00</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.290</s><span className='text-dark'>Rs.380</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 mt-2 ms-3'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default SaleAndBankOffer