import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AddItemsToHistory } from '../../Redux/Actions/Actions'
import { emptyOrderedItems } from '../../Redux/Actions/Actions'
import { Link } from 'react-router-dom'


export class Orderplaced extends Component {
    getQuantity = (id) => {
        let product = this.props.orderedItems.filter((item) => {
            return item.id == id
        })
        return product[0].number
    }

    TotalCostOfProducts = (id) => {
        let product = this.props.orderedItems.filter((item) => {
            return item.id == id
        })
        return parseInt(product[0].number) * parseInt(product[0].price)
    }

    GrossBill = () => {
        let totalBill = this.props.orderedItems.reduce((price, product) => {
            price = price + parseInt(product.price) * parseInt(product.number)
            return price
        }, 0)
        return totalBill
    }

    clearOrders = () => {
        this.props.emptyOrderedItems()
    }

    render() {
        if (this.props.orderedItems.length <= 0) {
            return <div className='container'>
                <div className='row'>
                    <div className='col-12 mt-5'>
                        <div className='w-25 mx-auto'>
                            <img className='w-50 mb-3' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSn6GZDPgrHPrXMeUR4EMUIXq3pv6wQXZXwFcWx2rbGczwjdIMDLsapsSDp8AcfCMSaa28&usqp=CAU' alt='noproducts' />
                        </div>
                        <h3>Currently You have No Products in your Cart</h3>
                        <h4>Please Add Products</h4>
                        <Link to='/displayproducts'>
                            <button className='btn btn-success text-light border border-0' style={{ backgroundColor: '#4c6020' }}>More Products</button>
                        </Link>
                    </div>
                </div>
            </div>
        }
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-12 mt-4 pt-3 '>
                        <div className='col-12 col-md-8 col-lg-6 mx-auto'>
                            <img src='https://www.thequickorder.in/assets/img/success.png' className='w-25' alt='success' />
                        </div>
                    </div>
                    <div className='col-12 mt-4'>
                        <h4 className=' text-success'><strong><span>Order Placed!. </span>ThankYou For Purchasing Our Products</strong></h4>
                        <p className='text-start fs-4 mt-5'>You will receive a confirmation mail to <mark>{this.props.UserInfo[0].Email}</mark></p>
                    </div>
                    <div className='col-12 mt-3'>
                        <h3 className='text-start ps-md-4'><strong>Summary</strong></h3>
                        <hr />
                        {this.props.orderedItems.map((product) => {
                            return (<div className="col-12 d-flex justify-content-start mb-5 d-flex flex-wrap" key={product.id}>
                                <div className='col-md-3 col-5 order-1 order-md-1 d-flex align-items-center justify-content-center'>
                                    <img className='w-50' src={product.img} alt='product' />
                                </div>
                                <div className='col-md-3 col-8 order-4 order-md-2 mt-3 mt-md-0  d-flex flex-column justify-content-center text-wrap text-start ps-4'>
                                    <h4 className='fs-6'>{product.title}</h4>
                                    <p className='d-none d-lg-block'><strong>Category: </strong>{product.category}</p>
                                    <h4 className='border border-1 w-75 text-center d-flex flex-nowrap align-items-center justify-content-center'>Qty:
                                        {this.getQuantity(product.id)}
                                    </h4>
                                </div>
                                <div className='col-md-3 col-3 order-2 order-md-3 d-flex flex-column justify-content-md-center justify-content-end'>
                                    <h3 className='fs-5'><strong>Price:</strong></h3>
                                    <h2 className='text-success fs-5'><strong>&#8377;{product.price}</strong></h2>
                                </div>
                                <div className='col-md-3 col-3 order-3 order-md-4 d-flex flex-column justify-content-md-center justify-content-end'>
                                    <h3 className='fs-5'><strong>Total:</strong></h3>
                                    <h2 className='text-success fs-5'><strong>&#8377;{this.TotalCostOfProducts(product.id)}</strong></h2>
                                </div>
                            </div>)
                        })}
                    </div>
                    <hr />
                    <div className='col-8'>
                    </div>
                    <div className='col-3 col-md-4 text-end'>
                        <h3 className='pe-4'><strong>Total: <span className='text-success fs-1'>&#8377;{this.GrossBill()}</span></strong></h3>
                    </div>
                    <div className='col-12'>
                        <h4 className='text-start ps-md-4'><strong>Delivery To</strong></h4>
                        <hr />
                        {this.props.UserAddress.map((address) => {
                            return <div key={`address`} className='text-start ps-md-4'>
                                <h4><strong>{address.Name}</strong></h4>
                                <p className='my-0'><strong>Address:</strong> {address.Address}</p>
                                <p className='my-0'>{address.City}, {address.State}</p>
                                <p className='my-0'>{address.PinCode}</p>
                                <p className='my-0'>{address.Mobile}</p>
                                <p>{address.Email}</p>
                            </div>
                        })}
                        <hr/>
                    </div>
                    <div className='col-12 text-start mt-3 ps-md-5'>
                        <Link to='/displayproducts'>
                            <button className='btn btn-success text-light border border-0' style={{ backgroundColor: '#4c6020' }} onClick={this.clearOrders}>
                                <strong><i className="fa-solid fa-basket-shopping text-dark fs-4 pe-2"></i>Explore More</strong>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cartItems,
        UserInfo: state.UserInfo,
        orderedItems: state.orderedItems,
        UserAddress: state.UserAddress
    }
}

const mapDispatchToProps = {
    AddItemsToHistory,
    emptyOrderedItems
}

export default connect(mapStateToProps, mapDispatchToProps)(Orderplaced)